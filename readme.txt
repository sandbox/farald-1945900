
Commerce "Add to cart" button
-----------------------------

This module allows you to use a button instead of a form to add products to the
cart. Enables you to fully cache more of the page, improving performance of
pages that show many "Add to cart" buttons.


--------------------------------------------------------------------------------
                                  Requirements
--------------------------------------------------------------------------------

  * Commerce (with submodules Cart, Product and Product Reference)


--------------------------------------------------------------------------------
                                  Installation
--------------------------------------------------------------------------------

  1. Install the module as normal

  2. Add the following lines to the site's robots.txt file:

     # Commerce "Add to cart" button
     Disallow: /commerce/add_to_cart_button
     Disallow: /?q=commerce/add_to_cart_button
